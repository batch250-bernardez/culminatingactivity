package com.zuitt.activity;

import java.util.ArrayList;

public class Phonebook {

    // Properties (ArrayList)
    public ArrayList<Contact> contacts = new ArrayList<Contact>();

    // Constructor
    public Phonebook() {
        contacts = new ArrayList<Contact>();
    }

    // Getter and Setter
    // Getter
    public ArrayList<Contact> getContacts() {
        return this.contacts;
    }

    // Setter
    public void setContacts(Contact contact) {
        contacts.add(contact);
    }




}
