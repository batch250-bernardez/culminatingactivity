import com.zuitt.activity.*;

public class Main {
    public static void main(String[] args) {

        // Instantiate new object
        Phonebook phonebook = new Phonebook();

        // Instantiate 2 contacts from the contact class
        Contact contactA = new Contact("John Doe", "+63123456789", "my home is in Quezon City");
        Contact contactB = new Contact("Jane Doe", "+63987654321", "my home is in Caloocan City");

        // Add both contacts to the phonebook using setContacts
        phonebook.setContacts(contactA);
        phonebook.setContacts(contactB);

        // Output notification message in the console
        if(phonebook.getContacts().isEmpty()){
            System.out.println("Empty: Phonebook has no contacts");
        }
        else{
            for(Contact contact : phonebook.getContacts()){
                printInfo(contact);
            }
        }
    }

    // Receive the contact parameter from Contact class
    public static void printInfo(Contact contact){
        System.out.println(contact.getName());
        System.out.println("------------------------------");

        if(contact.getContactNumber() != null){
            System.out.println(contact.getName() + " has the following registered number: " + contact.getContactNumber());
        }
        else {
            System.out.println(contact.getName() + "has no registered number");
        }

        if(contact.getAddress() != null){
            System.out.println(contact.getName() + " has the following registered address: " + contact.getAddress());
        }
        else{
            System.out.println(contact.getName() + "has no registered address");
        }
    }
}